//
//  Persistence.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RealmSwift
import RxSwift

final class Persistence: PersistenceType {
    
    func store<T>(_ values: [T], update: Bool) -> Observable<[T]> where T : Persistable {
        return Observable.create { observer in
            let realm = try! Realm()
            try! realm.write {
                values.forEach { value in
                    realm.add(value.managedObject(), update: update)
                }
            }
            observer.onNext(values)
            observer.onCompleted()
            return Disposables.create()
        }
    }
    
    func values<T: Persistable>(_ type: T.Type, matching query: NSPredicate? = nil) -> Observable<[T]> {
        return Observable.create { observer in
            let realm = try! Realm()
            var managedResults = realm.objects(T.ManagedObject.self)
            if let predicate = query {
                managedResults = managedResults.filter(predicate)
            }
            let results: [T] = managedResults.map { T(managedObject: $0) }
            observer.onNext(results)
            observer.onCompleted()
            return Disposables.create()
        }
    }
}
