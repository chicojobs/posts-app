//
//  PersistenceType.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift

protocol PersistenceType {
    func store<T: Persistable>(_ values: [T], update: Bool) -> Observable<[T]>
    func values<T: Persistable>(_ type: T.Type, matching query: NSPredicate?) -> Observable<[T]>
}
