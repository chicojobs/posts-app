//
//  PostsViewModel.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift
import RxCocoa

final class PostsViewModel: ViewModel, Routable {
    
    typealias RouterType = PostsRouter
    var router: PostsRouter?
    
    enum Routes {
        case list
        case detail(post: Post)
    }
    
    private let disposeBag = DisposeBag()
    
    // Output
    let cellViewModel: Driver<[PostCellViewModel]>
    var viewModelSelected: Driver<PostCellViewModel>! {
        didSet {
            if let viewModelSelected = self.viewModelSelected {
                viewModelSelected.drive(onNext: { [unowned self] detailViewModel in
                    self.router?.route(to: .detail(post: detailViewModel.post))
                }).disposed(by: disposeBag)
            }
        }
    }
    
    init(datasource: DatasourceType) {
        self.cellViewModel = datasource.fetch(entity: Post.self)
            .map { posts in return posts.map { PostCellViewModel(post: $0) } }
            .asDriver(onErrorJustReturn: [])
    }
}
