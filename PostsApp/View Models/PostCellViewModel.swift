//
//  PostCellViewModel.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import Foundation

class PostCellViewModel {
    let post: Post
    
    // Output
    var title: String { return post.title }
    var body: String { return post.body }
    
    init(post: Post) {
        self.post = post
    }
}
