//
//  DetailViewModel.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift
import RxCocoa

final class DetailViewModel {
    
    // output
    let post: Post
    var title: Driver<String>
    var authorName: Driver<String>
    var body: Driver<String>
    var numberOfComments: Driver<String>
    
    init(post: Post, datasource: DatasourceType) {
        let user = datasource
            .fetch(entity: User.self)
            .map { users in users.filter { $0.id == post.userId }.first }
        
        self.post = post
        authorName = user.map { $0?.name ?? "" }.asDriver(onErrorJustReturn: "")
        title = Driver.of(post.title)
        body = Driver.of(post.body)
        
        numberOfComments = datasource.fetch(entity: Comment.self)
            .map { comments in comments.filter { $0.postId == post.userId } }
            .map { return String($0.count) }
            .asDriver(onErrorJustReturn: "0")
    }
}
