//
//  ViewModel.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import Foundation

protocol ViewModel {
    associatedtype RouterType: Router
    var router: RouterType? { get set }
}
