//
//  DatasourceType.swift
//  PostsApp
//
//  Created by Enrico Querci on 20/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift

protocol DatasourceType {
    func fetch<T: Persistable>(entity: T.Type) -> Observable<[T]>
}
