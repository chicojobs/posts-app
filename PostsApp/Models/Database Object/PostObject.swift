//
//  PostObject.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RealmSwift

final class PostObject: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var title: String = ""
    @objc dynamic var body: String = ""
    @objc dynamic var userId: Int = 0
    @objc dynamic var user: UserObject?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
