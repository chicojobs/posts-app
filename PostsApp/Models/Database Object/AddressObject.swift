//
//  AddressObject.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RealmSwift

final class AddressObject: Object {
    @objc dynamic var uuid: String = ""
    @objc dynamic var street: String = ""
    @objc dynamic var suite: String = ""
    @objc dynamic var city: String = ""
    @objc dynamic var zip: String = ""
    @objc dynamic var geolocation: GeolocationObject?
    
    override static func primaryKey() -> String? {
        return "uuid"
    }
}
