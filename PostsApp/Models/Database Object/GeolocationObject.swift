//
//  GeolocationObject.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RealmSwift

final class GeolocationObject: Object {
    @objc dynamic var uuid: String = ""
    @objc dynamic var latitude: String = ""
    @objc dynamic var longitude: String = ""
    
    override static func primaryKey() -> String? {
        return "uuid"
    }
}
