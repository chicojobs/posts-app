//
//  CompanyObject.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RealmSwift

final class CompanyObject: Object {
    @objc dynamic var uuid: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var catchPhrase: String = ""
    @objc dynamic var bs: String = ""
    
    override static func primaryKey() -> String? {
        return "uuid"
    }
}
