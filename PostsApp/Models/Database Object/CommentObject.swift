//
//  CommentObject.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RealmSwift

final class CommentObject: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var body: String = ""
    @objc dynamic var postId: Int = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
