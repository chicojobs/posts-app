//
//  Datasource.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift

class Datasource: DatasourceType {
    let network: NetworkServiceType
    let persistence: PersistenceType
    
    init(network: NetworkServiceType, persistence: PersistenceType) {
        self.network = network
        self.persistence = persistence
    }
    
    func fetch<T: Persistable>(entity: T.Type) -> Observable<[T]> {
        return network
            .fetch(entity: [T].self, with: ApiService.request(for: entity))
            .flatMap { results in
                self.persistence.store(results, update: true)
            }
            .catchError { error in
                self.persistence.values(T.self, matching: nil)
        }
    }
}

