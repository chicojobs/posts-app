//
//  Post.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import Foundation

struct Post: Persistable {
    let id: Int
    let title: String
    let body: String
    let userId: Int
    var user: User?
}

extension Post {
    init(managedObject: PostObject) {
        id = managedObject.id
        title = managedObject.title
        body = managedObject.body
        userId = managedObject.userId
    }
    
    func managedObject() -> PostObject {
        let postObject = PostObject()
        
        postObject.id = id
        postObject.title = title
        postObject.body = body
        postObject.userId = userId
        
        return postObject
    }
}
