//
//  Address.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import Foundation

struct Address: Persistable {
    let street: String
    let suite: String
    let city: String
    let zip: String
    let geolocation: Geolocation?
    
    enum CodingKeys: String, CodingKey {
        case street = "street"
        case suite = "suite"
        case city = "city"
        case zip = "zipcode"
        case geolocation = "geo"
    }
}

extension Address {
    init(managedObject: AddressObject) {
        street = managedObject.street
        suite = managedObject.suite
        city = managedObject.city
        zip = managedObject.zip
        geolocation = managedObject.geolocation.flatMap { return Geolocation(managedObject: $0) }
    }
    
    func managedObject() -> AddressObject {
        let addressObject = AddressObject()
        addressObject.street = street
        addressObject.suite = suite
        addressObject.city = city
        addressObject.zip = zip
        addressObject.geolocation = geolocation?.managedObject()
        addressObject.uuid = street+suite+city+zip
        
        return addressObject
    }
}
