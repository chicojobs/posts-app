//
//  Geolocation.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import Foundation

struct Geolocation: Persistable {
    let latitude: String
    let longitude: String
    
    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lng"
    }
}

extension Geolocation {
    init(managedObject: GeolocationObject) {
        latitude = managedObject.latitude
        longitude = managedObject.longitude
    }
    
    func managedObject() -> GeolocationObject {
        let geolocationObject = GeolocationObject()
        geolocationObject.latitude = self.latitude
        geolocationObject.longitude = self.longitude
        geolocationObject.uuid = latitude+longitude
        
        return geolocationObject
    }
}
