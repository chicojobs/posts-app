//
//  User.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import Foundation

struct User: Persistable {
    let id: Int
    let name: String
    let username: String
    let email: String
    let address: Address?
    let phone: String
    let website: String
    let company: Company?
}

extension User {
    init(managedObject: UserObject) {
        id = managedObject.id
        name = managedObject.name
        username = managedObject.username
        email = managedObject.email
        address = managedObject.address.flatMap { return Address(managedObject: $0) }
        phone = managedObject.phone
        website = managedObject.website
        company = managedObject.company.flatMap { return Company(managedObject: $0) }
    }
    
    func managedObject() -> UserObject {
        let userObject = UserObject()
        userObject.id = id
        userObject.name = name
        userObject.username = username
        userObject.email = email
        userObject.address = address?.managedObject()
        userObject.phone = phone
        userObject.website = website
        userObject.company = company?.managedObject()
        
        return userObject
    }
}
