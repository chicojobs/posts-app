//
//  Company.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import Foundation

struct Company: Persistable {
    let name: String
    let catchPhrase: String
    let bs: String
}

extension Company {
    init(managedObject: CompanyObject) {
        name = managedObject.name
        catchPhrase = managedObject.catchPhrase
        bs = managedObject.bs
    }
    
    func managedObject() -> CompanyObject {
        let companyObject = CompanyObject()
        companyObject.name = name
        companyObject.catchPhrase = catchPhrase
        companyObject.bs = bs
        companyObject.uuid = name+catchPhrase+bs
        
        return companyObject
    }
}
