//
//  Comment.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import Foundation

struct Comment: Persistable {
    let id: Int
    let name: String
    let email: String
    let body: String
    let postId: Int
}

extension Comment {
    init(managedObject: CommentObject) {
        id = managedObject.id
        name = managedObject.name
        email = managedObject.email
        body = managedObject.body
        postId = managedObject.postId
    }
    
    func managedObject() -> CommentObject {
        let commentObject = CommentObject()
        commentObject.id = id
        commentObject.name = name
        commentObject.email = email
        commentObject.body = body
        commentObject.postId = postId
        
        return commentObject
    }
}
