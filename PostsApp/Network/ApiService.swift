//
//  ApiService.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import Foundation

struct ApiService {
    static let baseURL: String = "http://jsonplaceholder.typicode.com"
    
    static func request<T: Codable>(for type: T.Type) -> URLRequest {
        
        var path: String!
        
        switch type {
        case is Post.Type:
            path = "/posts"
        case is User.Type:
            path = "/users"
        case is Comment.Type:
            path = "/comments"
        default:
            fatalError("No path provided")
        }
        
        let url = URL(string: ApiService.baseURL + path)!
        return URLRequest(url: url)
    }
}
