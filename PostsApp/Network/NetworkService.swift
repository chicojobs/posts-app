//
//  NetworkService.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift
import RxCocoa

class NetworkService: NetworkServiceType {
    
    lazy var session: URLSession = {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        return session
    }()
    
    func fetch<T: Codable>(entity: T.Type, with request: URLRequest) -> Observable<T> {
        return session.rx.data(request: request)
            .flatMap { data -> Observable<T> in
                let decoder = JSONDecoder()
                do {
                    let result = try decoder.decode(T.self, from: data)
                    return Observable.of(result)
                } catch let error {
                    return Observable.error(error)
                }
        }
    }
}
