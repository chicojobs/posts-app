//
//  NetworkServiceType.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift

protocol NetworkServiceType {
    func fetch<T: Codable>(entity: T.Type, with request: URLRequest) -> Observable<T>
}
