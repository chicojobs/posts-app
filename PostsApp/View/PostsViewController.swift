//
//  PostsViewController.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift
import RxCocoa

class PostsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let viewModel: PostsViewModel
    private let disposeBag = DisposeBag()
    
    init(with viewModel: PostsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Posts"
        
        let cellNib = UINib(nibName: String(describing: PostTableViewCell.self), bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: PostTableViewCell.identifier)
        
        viewModel.cellViewModel
            .drive(tableView.rx.items(cellIdentifier: PostTableViewCell.identifier, cellType: PostTableViewCell.self))
            { row, cellViewModel, cell in
                cell.setup(with: cellViewModel)
            }
            .disposed(by: disposeBag)
        
        viewModel.viewModelSelected = tableView.rx.modelSelected(PostCellViewModel.self).asDriver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let indexPath = tableView.indexPathForSelectedRow else { return }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
