//
//  DetailViewController.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift

class DetailViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var numberOfCommentsLabel: UILabel!
    
    private let viewModel: DetailViewModel
    private let disposeBag = DisposeBag()
    
    init(with viewModel: DetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .never
        title = "Detail"
        
        viewModel.title.drive(titleLabel.rx.text).disposed(by: disposeBag)
        viewModel.authorName.drive(authorLabel.rx.text).disposed(by: disposeBag)
        viewModel.body.drive(bodyLabel.rx.text).disposed(by: disposeBag)
        viewModel.numberOfComments.drive(numberOfCommentsLabel.rx.text).disposed(by: disposeBag)
    }
}
