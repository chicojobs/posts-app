//
//  PostTableViewCell.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    
    static let identifier: String = "PostTableViewCell Identifier"
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
    func setup(with viewModel: PostCellViewModel) {
        titleLabel.text = viewModel.title
        bodyLabel.text = viewModel.body
    }
}
