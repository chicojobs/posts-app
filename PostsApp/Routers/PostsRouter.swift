//
//  PostsRouter.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import UIKit

class PostsRouter: Router {
    typealias Context = UINavigationController
    typealias RoutableType = PostsViewModel
    weak var context: Context?
    
    func route(to route: PostsViewModel.Routes) {
        switch route {
        case .detail(let post):
            let network: NetworkServiceType = NetworkService()
            let persistence: PersistenceType = Persistence()
            let datasource = Datasource(network: network, persistence: persistence)
            let detailViewModel = DetailViewModel(post: post, datasource: datasource)
            let detailViewController = DetailViewController(with: detailViewModel)
            context?.pushViewController(detailViewController, animated: true)
        default:
            break
        }
    }
}
