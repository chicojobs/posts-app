//
//  AppDelegate.swift
//  PostsApp
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    let startViewController: UINavigationController = {
        let persistence = Persistence()
        let network = NetworkService()
        let datasource = Datasource(network: network, persistence: persistence)
        
        let viewModel = PostsViewModel(datasource: datasource)
        let viewController = PostsViewController(with: viewModel)
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.prefersLargeTitles = true
        
        let router = PostsRouter()
        router.context = navigationController
        viewModel.router = router
        
        return navigationController
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        self.window?.rootViewController = startViewController
        self.window?.makeKeyAndVisible()
        
        return true
    }
}
