# PostsApp
This simple iOS app displays a list of Posts and a detail view containing additional data for each of them (number of comments, author name)

This is mainly an exercise to show the use of **MVVM** pattern combined with `RxSwift`.
It also features a **router** element which takes care of the navigation and allows the developer to avoid any type of storyboard-based navigation.

The goal is to achieve a better separation between data model, presentation, persistance, navigation and business logic as well as improving general testability, resorting to **protocol-oriented programming** and **dependency injection**.

The app is written in `Swift 4` and uses the `Codable`protocol.

## Requirements
* Xcode 9 or higher
* Swift 4
* Cocoapods

# Attribution
The data layer is heavily inspired by [this article](https://medium.com/@gonzalezreal/using-realm-with-value-types-b69947741e8b) from [Guillermo Gonzalez](https://github.com/gonzalezreal).
The idea is to create a wrapper around your preferred persistence technology (`Realm` in this case) and use `struct` based models.

## License
PostsApp is released under the MIT license. See LICENSE for details.
