//
//  PersistenceMock.swift
//  PostsAppTests
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift
@testable import PostsApp

class PersistenceMock: PersistenceType {
    func store<T>(_ values: [T], update: Bool) -> Observable<[T]> where T : Persistable {
        return Observable.of(values)
    }
    
    func values<T>(_ type: T.Type, matching query: NSPredicate?) -> Observable<[T]> where T : Persistable {
        
        var mockData: [T]
        
        switch type {
        case is [Post].Type:
            mockData = TestHelper.postsArray() as! [T]
        case is [User].Type:
            mockData = TestHelper.usersArray() as! [T]
        case is [Comment].Type:
            mockData = TestHelper.commentsArray() as! [T]
        default:
            fatalError()
        }

        return Observable.of(mockData)
    }
}
