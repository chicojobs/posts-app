//
//  NetworkMock.swift
//  PostsAppTests
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import RxSwift
import RxTest
@testable import PostsApp

class NetworkMock: NetworkServiceType {
    let scheduler: TestScheduler

    init(scheduler: TestScheduler) {
        self.scheduler = scheduler
    }
    
    func fetch<T>(entity: T.Type, with request: URLRequest) -> Observable<T> where T : Decodable, T : Encodable {
        
        var mockData: T
        
        switch entity {
        case is [Post].Type:
            mockData = TestHelper.postsArray() as! T
        case is [User].Type:
            mockData = TestHelper.usersArray() as! T
        case is [Comment].Type:
            mockData = TestHelper.commentsArray() as! T
        default:
            fatalError()
        }
        
        let testObservable = scheduler.createColdObservable([
            next(0, mockData),
            completed(100)
            ])
        return testObservable.asObservable()
    }
}
