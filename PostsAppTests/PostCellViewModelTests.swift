//
//  PostCellViewModelTests.swift
//  PostsAppTests
//
//  Created by Enrico Querci on 20/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa
import RxTest
@testable import PostsApp

class PostCellViewModelTests: XCTestCase {
    
    var sampleUser: User!
    var samplePost: Post!
    
    let scheduler = TestScheduler(initialClock: 0)
    let disposeBag = DisposeBag()
    var viewModel: PostCellViewModel!
    
    override func setUp() {
        super.setUp()
        sampleUser = User(id: 1, name: "Paul McCartney", username: "macca", email: "paul@mplcommunications", address: nil, phone: "020 7439 2001", website: "https://mplcommunications.com/", company: nil)
        samplePost = Post(id: 1, title: "Sample post", body: "This is a sample post", userId: 1, user: sampleUser)
        
        viewModel = PostCellViewModel(post: samplePost)
    }

    func testPostCellViewModelData() {
        XCTAssertEqual(viewModel.title, "Sample post")
        XCTAssertEqual(viewModel.body, "This is a sample post")
    }
}
