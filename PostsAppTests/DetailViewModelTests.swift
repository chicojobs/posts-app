//
//  DetailViewModelTests.swift
//  PostsAppTests
//
//  Created by Enrico Querci on 20/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa
import RxTest
@testable import PostsApp

class DetailViewModelTests: XCTestCase {
    
    let scheduler = TestScheduler(initialClock: 0)
    let disposeBag = DisposeBag()
    var datasource: DatasourceType!
    var viewModel: DetailViewModel!
    
    var sampleUser: User!
    var samplePost: Post!
    var sampleComments: [Comment]!
    
    override func setUp() {
        super.setUp()
        
        let network = NetworkMock(scheduler: scheduler)
        let persistence = PersistenceMock()
        datasource = Datasource(network: network, persistence: persistence)
        setupMockData()
        viewModel = DetailViewModel(post: samplePost, datasource: datasource)
    }
    
    func setupMockData() {
        sampleUser = TestHelper.usersArray().first!
        samplePost = TestHelper.postsArray().first!
        sampleComments = Array(TestHelper.commentsArray().prefix(5))
    }
    
    func testDetailViewModelData() {
        SharingScheduler.mock(scheduler: scheduler) {
            let titleObserver = scheduler.createObserver(String.self)
            let authorNameObserver = scheduler.createObserver(String.self)
            let bodyObserver = scheduler.createObserver(String.self)
            let numberOfCommentsObservable = scheduler.createObserver(String.self)
            
            viewModel.title.drive(titleObserver).disposed(by: disposeBag)
            viewModel.authorName.drive(authorNameObserver).disposed(by: disposeBag)
            viewModel.body.drive(bodyObserver).disposed(by: disposeBag)
            viewModel.numberOfComments.drive(numberOfCommentsObservable).disposed(by: disposeBag)

            scheduler.start()
            XCTAssertEqual(bodyObserver.events.count, 1)

            let titleResult = titleObserver.events.map { $0.value.element! }.first!
            let authorNameResult = authorNameObserver.events.first!.value.element!
            let bodyResult = bodyObserver.events.map { $0.value.element! }.first!
            let numberOfCommentsResult = numberOfCommentsObservable.events.first!.value.element!
            
            XCTAssertEqual(titleResult, samplePost.title)
            XCTAssertEqual(authorNameResult, sampleUser.name)
            XCTAssertEqual(bodyResult, samplePost.body)
            XCTAssertEqual(numberOfCommentsResult, String(sampleComments.count))
        }
    }
}

