//
//  PostsAppTests.swift
//  PostsAppTests
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
@testable import PostsApp

class NetworkTests: XCTestCase {
    
    let scheduler = TestScheduler(initialClock: 0)
    let disposeBag = DisposeBag()
    var datasource: DatasourceType!
    
    override func setUp() {
        super.setUp()
        let network = NetworkMock(scheduler: scheduler)
        let persistence = PersistenceMock()
        datasource = Datasource(network: network, persistence: persistence)

    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFetchPosts() {
        let observer = scheduler.createObserver([Post].self)

        scheduler.scheduleAt(0) {
            self.datasource.fetch(entity: Post.self).subscribe(observer).disposed(by: self.disposeBag)
        }
        scheduler.start()

        let expected = [
            next(100, TestHelper.postsArray()),
            completed(200)
        ]

        XCTAssertEqual(observer.events.count, expected.count)
        XCTAssertEqual(observer.events.first?.value.element?.count, expected.first?.value.element?.count)
        XCTAssertEqual(observer.events.first?.value.element?.first?.id, expected.first?.value.element?.first?.id)
    }

    func testFetchUsers() {
        let observer = scheduler.createObserver([User].self)
        
        scheduler.scheduleAt(0) {
            self.datasource.fetch(entity: User.self).subscribe(observer).disposed(by: self.disposeBag)
        }
        scheduler.start()
        
        let expected = [
            next(100, TestHelper.usersArray()),
            completed(200)
        ]
        
        XCTAssertEqual(observer.events.count, expected.count)
        XCTAssertEqual(observer.events.first?.value.element?.count, expected.first?.value.element?.count)
        XCTAssertEqual(observer.events.first?.value.element?.first?.id, expected.first?.value.element?.first?.id)
    }
    
    func testFetchComments() {
        let observer = scheduler.createObserver([Comment].self)
        
        scheduler.scheduleAt(0) {
            self.datasource.fetch(entity: Comment.self).subscribe(observer).disposed(by: self.disposeBag)
        }
        scheduler.start()
        
        let expected = [
            next(100, TestHelper.commentsArray()),
            completed(200)
        ]
        
        XCTAssertEqual(observer.events.count, expected.count)
        XCTAssertEqual(observer.events.first?.value.element?.count, expected.first?.value.element?.count)
        XCTAssertEqual(observer.events.first?.value.element?.first?.id, expected.first?.value.element?.first?.id)
    }

}
