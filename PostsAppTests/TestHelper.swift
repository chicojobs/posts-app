//
//  TestHelper.swift
//  PostsAppTests
//
//  Created by Enrico Querci on 19/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import XCTest
@testable import PostsApp

class TestHelper: XCTestCase {
    let baseURL = "http://jsonplaceholder.typicode.com"
    static let bundle = Bundle(for: TestHelper.self)
    
    static func postsArray() -> [Post] {
        let localURL = URL(fileURLWithPath: bundle.path(forResource: "posts", ofType: "json")!)
        let data = try! Data(contentsOf: localURL)
        
        let posts = try! JSONDecoder().decode([Post].self, from: data)
        return posts
    }
 
    static func usersArray() -> [User] {
        let localURL = URL(fileURLWithPath: bundle.path(forResource: "users", ofType: "json")!)
        let data = try! Data(contentsOf: localURL)
        
        let posts = try! JSONDecoder().decode([User].self, from: data)
        return posts
    }

    static func commentsArray() -> [Comment] {
        let localURL = URL(fileURLWithPath: bundle.path(forResource: "comments", ofType: "json")!)
        let data = try! Data(contentsOf: localURL)
        
        let posts = try! JSONDecoder().decode([Comment].self, from: data)
        return posts
    }
}
