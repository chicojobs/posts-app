//
//  PostsViewModelTests.swift
//  PostsAppTests
//
//  Created by Enrico Querci on 20/11/2017.
//  Copyright © 2017 Enrico Querci. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa
import RxTest
@testable import PostsApp

class PostsViewModelTests: XCTestCase {
    
    let scheduler = TestScheduler(initialClock: 0)
    let disposeBag = DisposeBag()
    var datasource: DatasourceType!
    var viewModel: PostsViewModel!
    
    override func setUp() {
        super.setUp()
        
        let network = NetworkMock(scheduler: scheduler)
        let persistence = PersistenceMock()
        datasource = Datasource(network: network, persistence: persistence)
        viewModel = PostsViewModel(datasource: datasource)
        
    }

    func testPostViewModelData() {
        SharingScheduler.mock(scheduler: scheduler) {
            let observer = scheduler.createObserver([PostCellViewModel].self)
            viewModel.cellViewModel.drive(observer).disposed(by: disposeBag)
            scheduler.start()
            XCTAssertEqual(observer.events.count, 2)
            XCTAssertEqual(observer.events.first?.value.element?.count, 100)
        }
    }
}
